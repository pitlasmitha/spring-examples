package examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import examples.postgres.entities.*;
import examples.mongo.entities.*;
import examples.mongo.repositories.BoardRepository;
import examples.postgres.repositories.CustomerRepository;
import examples.storage.StorageProperties;
import examples.storage.StorageService;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
@EnableConfigurationProperties(StorageProperties.class)
@ComponentScan(basePackages = "examples")
public class Application {
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	final static String queueName = "spring-boot";

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("spring-boot-exchange");
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(queueName);
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
            MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    Receiver receiver() {
        return new Receiver();
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }
	
    public static void main(String[] args) {
    	//RestTemplate restTemplate = new RestTemplate();
        // quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
        //log.info(quote.toString());
    	SpringApplication.run(Application.class, args);
    }
    
    // Use commented code if anything should be run once on start up
    
    /*@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			Quote quote = restTemplate.getForObject(
					"http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
			log.info(quote.toString());
		};
	}*/
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    //@Override   override only if Application implements CommandLineRunner 
    /*@Bean   // @Bean not needed if Application implements CommandLineRunner
    // If run method needs any parameters they should be defined as beans first as shown above and then passed to run method
    public CommandLineRunner run(StorageService storageService) throws Exception {

        return args -> { 
        	log.info("Creating tables");
       

        jdbcTemplate.execute("DROP TABLE customers IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE customers(" +
                "id SERIAL, first_name VARCHAR(255), last_name VARCHAR(255))");

        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("John Woo", "Jeff Dean", "Josh Bloch", "Josh Long").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Inserting customer record for %s %s", name[0], name[1])));

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO customers(first_name, last_name) VALUES (?,?)", splitUpNames);

        log.info("Querying for customer records where first_name = 'Josh':");
        jdbcTemplate.query(
                "SELECT id, first_name, last_name FROM customers WHERE first_name = ?", new Object[] { "Josh" },
                (rs, rowNum) -> new Employee(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
        ).forEach(customer -> log.info(customer.toString()));
        
        // Uncomment below lines for first time,  to create upload dir
        storageService.deleteAll();
        storageService.init();

        
        };
    }*/
    
    
    @Bean
	public CommandLineRunner demo(CustomerRepository repository, BoardRepository boardRepository) {
		return (args) -> {
			
			//Postgres start
			// save a couple of customers
			repository.save(new Customer("Jack", "Bauer"));
			repository.save(new Customer("Chloe", "O'Brian"));
			repository.save(new Customer("Kim", "Bauer"));
			repository.save(new Customer("David", "Palmer"));
			repository.save(new Customer("Michelle", "Dessler"));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
            log.info("");

			// fetch an individual customer by ID
            Customer customer = repository.findOne(1L);
			log.info("Customer found with findOne(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
            log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			for (Customer bauer : repository.findByLastName("Bauer")) {
				log.info(bauer.toString());
			}
            log.info("");
            // Postgres end
            // Mongo start
            boardRepository.save(new Board("BKN01", "Brooklyn"));
            boardRepository.save(new Board("MN02", "Manhattan"));
            boardRepository.save(new Board("MN01", "Manhattan"));

    		// fetch all Boards
    		System.out.println("Boards found with findAll():");
    		System.out.println("-------------------------------");
    		for (Board board : boardRepository.findAll()) {
    			System.out.println(board);
    		}
    		System.out.println();

    		// fetch an individual board
    		System.out.println("Board found with findByName('BKN01'):");
    		System.out.println("--------------------------------");
    		System.out.println(boardRepository.findByName("BKN01"));

    		System.out.println("Boards found with findByLocation('Manhattan'):");
    		System.out.println("--------------------------------");
    		for (Board board : boardRepository.findByLocation("Manhattan")) {
    			System.out.println(board);
    		}
            // Mongo end
		};
	}
}
