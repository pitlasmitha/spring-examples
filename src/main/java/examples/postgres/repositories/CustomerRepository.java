package examples.postgres.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import examples.postgres.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	List<Customer> findByLastName(String lastName);

}
