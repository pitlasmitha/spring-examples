package examples;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
    // Example of consuming a web service and also scheduling a task
    @Scheduled(fixedRate = 20000)
    public void reportCurrentTimeAndRandomQuote() { // only no  argument methods may be annotated as Scheduled
    		Quote quote = new RestTemplate().getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
			log.info("The time is now {}", dateFormat.format(new Date())+" And Quote is::"+quote.getValue().getQuote());
    }
}
