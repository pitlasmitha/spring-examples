package examples.mongo.entities;

import org.springframework.data.annotation.Id;

public class Board {
	
	@Id
    public String id;

    public String name;
    public String location;

    public Board() {}

    public Board(String name, String location) {
        this.name = name;
        this.location = location;
    }

    @Override
    public String toString() {
        return String.format(
                "Board[id=%s, name='%s', location='%s']",
                id, name, location);
    }

}
