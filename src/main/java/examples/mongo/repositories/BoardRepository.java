package examples.mongo.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import examples.mongo.entities.Board;

public interface BoardRepository extends MongoRepository<Board, String>{
	
	public Board findByName(String name);
    public List<Board> findByLocation(String location);

}
